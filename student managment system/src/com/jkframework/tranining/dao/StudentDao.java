package com.jkframework.tranining.dao;

import com.jkframework.tranining.db.DbConnection;
import com.jkframework.tranining.entity.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class StudentDao implements StudentInterface{
    Student student1 ;
    ResultSet resul;
    //this method add student in database
    @Override
    public boolean insertStudent(Student s) {
        boolean checkInsertedOrNot = false;
        try {
            Connection connection= DbConnection.createConnection();
            String query = "insert into student (name,average,email) value(?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1,s.getName());
            preparedStatement.setDouble(2,s.getAverage());
            preparedStatement.setString(3,s.getEmail());
            preparedStatement.executeUpdate();
            checkInsertedOrNot = true;

        }catch (Exception e){
            e.getMessage();
        }
        return checkInsertedOrNot;
    }

    // this method print all student in 0
    @Override
    public void printAllStudent() {
        ArrayList<Student> students = getAllStudentFromDataBase();
        for (Student s:students){
            System.out.println("id number for student = "+s.getId()+"\n"+
                    "name of student = "+s.getName()+"\n"+
                    "average of student = "+s.getAverage()+"\n"+
                    "email of student = "+s.getEmail());
            System.out.println("-------------------------------------------------------------------------------");
        }
    }


    // this method update information student in database
    @Override
    public boolean updateStudentById(int idStudent) {
        boolean check = false;
        Scanner scanner = new Scanner(System.in);
        check =  showStudentById(idStudent);
        if (check){
            System.out.println("\n1.update name"+
                    "\n2.update average"+
                    "\n3.ubdate email"+
                    "\n4.update for all"+
                    "\n5.exit");
            System.out.println("-------------------------------------------------------------------------------");
            System.out.println("enter your choice");
            int inputFromUser = scanner.nextInt();
            switch (inputFromUser){
                case 1:
                    System.out.println("update name");
                    System.out.println("please enter new name ");
                    String nameUpdate = scanner.next();
                    update(nameUpdate);
                    break;
                case 2:
                    System.out.println("update average");
                    System.out.println("please enter new average ");
                    double averageUpdate = scanner.nextDouble();
                    update(averageUpdate);
                    break;
                case 3:
                    System.out.println("update email");
                    System.out.println("please enter new email ");
                    String emailUpdate = scanner.next();
                    update(student1.getId(),emailUpdate);
                    break;
                case 4:
                    System.out.println("update for all");
                    System.out.println("please enter new name ");
                    String nameU = scanner.next();
                    System.out.println("please enter new average ");
                    double averageU = scanner.nextDouble();
                    System.out.println("please enter new email ");
                    String emailUe = scanner.next();
                    update(nameU,averageU,emailUe);
                    break;
                case 5:
                    System.out.println("thank you for use Student managment system");
                    System.exit(0);
                    break;
                default:
                    System.out.println("sorry this choice not found");

            }}

        return check;
    }

    private void update(String nameUpdate)  {
        try {
            Connection connection = DbConnection.createConnection();
            String query = "update student set name=? where id=?";
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, nameUpdate);
            pst.setInt(2, student1.getId());
            pst.executeUpdate();
        }catch (Exception e ){

        }
    }
    private void update(double average)  {
        try {
            Connection connection = DbConnection.createConnection();
            String query = "update student set average=? where id=?";
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setDouble(1,average);
            pst.setInt(2,student1.getId());
            pst.executeUpdate();
        }catch (Exception e ){

        }
    }
    private void update(int id, String email)  {
        try {
            Connection connection = DbConnection.createConnection();
            String query = "update student set email=? where id=?";
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1,email);
            pst.setInt(2,id);
            pst.executeUpdate();
        }catch (Exception e ){

        }
    }
    private void update (String name,double average,String email)  {
        try {
            Connection connection = DbConnection.createConnection();
            String query = "update student set name=?,average=?,email=? where id=?";
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1,name);
            pst.setDouble(2,average);
            pst.setString(3,email);
            pst.setInt(4,student1.getId());
            pst.executeUpdate();
        }catch (Exception e ){

        }
    }

    //this method delete student from database
    @Override
    public boolean deleteStudentById(int idStudent) {
        boolean check = false;
        try {


            check = showStudentById(idStudent);
            Connection connection = DbConnection.createConnection();
            String query = "delete from student where id="+ idStudent;
            PreparedStatement pst = connection.prepareStatement(query);
            pst.executeUpdate();

        }
        catch (Exception e ){
            e.printStackTrace();
        }
        return check;
    }

    // this method return information about student by id
    private boolean showStudentById(int idStudent) {
        boolean check = false;
        try {
            Connection connection = DbConnection.createConnection();
            String query = "select * from student where id="+idStudent;
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                student1 =new Student(resultSet.getInt(1),resultSet.getString(2),
                        resultSet.getDouble(3),resultSet.getString(4));

                System.out.println("id number for student = " + resultSet.getInt(1) + "\n" +
                        "name of student = " + resultSet.getString(2) + "\n" +
                        "average of student = " + resultSet.getDouble(3) + "\n" +
                        "email of student = " + resultSet.getString(4)+"\n");
                check =true;
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return check;
    }

    // this method get all student from database and saveing on ArrayList
    private ArrayList<Student> getAllStudentFromDataBase() {
        ArrayList<Student> studentArrayList = new ArrayList<>();
        try {
            Connection connection = DbConnection.createConnection();
            String query = "select * from student";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet == null){
                System.out.println("sorry not found any student of system");
            }else {
                while (resultSet.next()) {
                    studentArrayList.add(   new Student(resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getDouble(3),
                            resultSet.getString(4)));
                }}
        }catch (Exception e){
            e.printStackTrace();
        }
        return studentArrayList;
    }
}