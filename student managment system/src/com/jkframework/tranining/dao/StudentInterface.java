package com.jkframework.tranining.dao;

import com.jkframework.tranining.entity.Student;

public interface StudentInterface {
    boolean insertStudent(Student s);
    void printAllStudent();
    boolean updateStudentById(int idStudent);
    boolean deleteStudentById(int idStudent);


}
