package com.jkframework.tranining.entity;

public class Student {

    private int id;
    private String name;
    private double average;
    private String email;

    public Student() {
    }

    public Student(String name, double average, String email) {
        this.name = name;
        this.average = average;
        this.email = email;
    }

    public Student(int id, String name, double average, String email) {
        this.id = id;
        this.name = name;
        this.average = average;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", average=" + average +
                ", email='" + email + '\'' +
                '}';
    }
}
