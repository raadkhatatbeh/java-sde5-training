import com.jkframework.tranining.dao.StudentDao;
import com.jkframework.tranining.dao.StudentInterface;
import com.jkframework.tranining.entity.Student;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        StudentInterface studentInterface = new StudentDao();
        Scanner scanner = new Scanner(System.in);
        System.out.println(" Welcom toStudent Managment Appplication");

        while (true){
            getMasegeForUser();
            int inputFromUser = scanner.nextInt();
            switch (inputFromUser){
                case 1:
                    System.out.println("Add Student");
                    System.out.println("enter Student name");
                    String name =scanner.next();

                    System.out.println("enter Student average");
                    double average = scanner.nextDouble();

                    System.out.println("enter Student Email");
                    String email = scanner.next();

                    Student student = new Student(name,average,email);
                    boolean checkAddStudent = studentInterface.insertStudent(student);
                    getMasegeForuserIfInsertedOrNot(checkAddStudent);
                    break;

                case 2:
                    System.out.println("Update Student By Id ");
                    System.out.println("please enter the id for student");
                    int idStudent = scanner.nextInt();
                    boolean check = studentInterface.updateStudentById(idStudent);
                    if(check == false)
                        System.out.println("woring not found student by id := "+idStudent);
                    else System.out.println("the student id updated");
                    break;
                case 3:
                    System.out.println("delete student by id ");
                    System.out.println("please enter the id for student");
                    idStudent = scanner.nextInt();
                    check = studentInterface.deleteStudentById(idStudent);
                    if(check == false)
                        System.out.println("woring not found student by id  :="+idStudent);
                    else System.out.println("the student is deleted !!.....");
                    break;
                case 4:
                    System.out.println("show All student");
                    studentInterface.printAllStudent();
                    break;
                case 5:
                    System.out.println("Thank you for using Student Managment Application!!");
                    System.exit(0);

                default:
                    System.out.println("please enter valid choice");
            }
        }

    }
    // this method return massege for user student is add or not
    private static void getMasegeForuserIfInsertedOrNot(boolean checkAddStudent) {
        if (checkAddStudent)
            System.out.println("Record inserted successfully!!");
        else
            System.out.println("woring! please try again");
    }

    // this method print comandline for user
    private static void getMasegeForUser() {
        System.out.println("\n1.Add Student"+
                "\n2.update student by id"+
                "\n3.delete student by id"+
                "\n4.Show All Student "+
                "\n5.Exit");
        System.out.println("please enter choice");
    }
}